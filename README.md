# Yourpass Writer

This component allows you to update, create, and delete passes using the YourPass API.
**Table of contents:**  

To create passes your input should contain the templateId, and optionally dynamicData, dynamicImages, and expirationDate
  
[TOC]

## Authorization
This component utilizes the password credentials method of authorization. To be able to use the writer the following must be 
included in the authorization configuration: 

- Username (username) - [REQ]
- Password (#password) - [REQ]
- Client Id (client_id) - [REQ]
- Client secret (#client_secret) - [OPT] If not specified, can be left empty

Additionally, you can also input if you want to write to sandbox mode 

- Sandbox (sandbox) - [OPT] checkbox of whether to use the sandbox mode URI

## Row configuration
In each row configuration only two variables are set :

- Endpoint (endpoint) - [REQ] Which endpoint to perform the action on ; pass, template ..
- Action (action) - [REQ] what action to perform; create, update, delete
