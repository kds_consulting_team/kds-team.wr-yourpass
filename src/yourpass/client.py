import base64
import logging
import requests
from typing import List
from retry import retry
from flatten_json import unflatten

PROD_URL = "https://api.yourpass.eu/"
SANDBOX_URL = "https://pass.ysplay.cz/api/"
AUTH_ENDPOINT = "oauth2/token"
PASS_ENDPOINT = "v1/pass"
BATCH_ENDPOINT = "v1/pass/batch"
ACCESS_TOKEN_KEY = "access_token"

PASS_KEYS = ['id', 'url', "dynamicData", "dynamicImages", 'voided', 'expirationDate', 'createdAt', 'updatedAt',
             'deletedAt', 'firstRegisterAt', 'lastRegisterAt', 'firstUnregisterAt', 'lastUnregisterAt', 'templateId',
             'projectId', 'devices']
UPDATE_KEYS = ['id', "dynamicData", "dynamicImages", "templateId", 'projectId']


class LoginError(Exception):
    pass


class YourPassClient:
    def __init__(self, sandbox):
        self.api_url = PROD_URL
        if sandbox:
            self.api_url = SANDBOX_URL
        self.access_token = ""
        self.request_header = {}

    def authenticate_client(self, client_id: str, client_secret: str, username: str, password: str):
        self.access_token = self.get_access_token(client_id, username, password, client_secret)
        self.request_header = self.get_request_header()

    def get_request_header(self) -> dict:
        auth_header = " ".join(["Bearer", self.access_token])
        header = {"authorization": auth_header,
                  "content-type": "application/json"}
        return header

    def get_access_token(self, client_id: str, username: str, password: str, client_secret: str):
        client_header = self.get_client_header(client_id, client_secret)
        header = {"Authorization": client_header,
                  "Cache-Control": "no-cache",
                  "Content-Type": "application/x-www-form-urlencoded"}
        data = {"grant_type": "password",
                "username": username,
                "password": password}
        auth_url = "".join([self.api_url, AUTH_ENDPOINT])
        auth_request = requests.post(auth_url, headers=header, data=data)
        if auth_request.status_code == 200:
            response = auth_request.json()
            return response[ACCESS_TOKEN_KEY]
        elif auth_request.status_code == 400:
            raise LoginError(
                "Failed to authorize extraction, please recheck your password, username, client Id and secret")

    @staticmethod
    def get_client_header(client_id: str, client_secret: str):
        client_header = ":".join([client_id, client_secret])
        encoded_client_header = YourPassClient.encode_to_base_64(client_header)
        return " ".join(["Basic", encoded_client_header])

    @staticmethod
    def encode_to_base_64(string_to_encode: str):
        string_to_encode = string_to_encode.encode('ascii')
        encoded_string = base64.b64encode(string_to_encode).decode('ascii')
        return encoded_string

    @staticmethod
    def unflatten_dynamic_data(yp_pass: dict):
        new_dict = unflatten(yp_pass)
        return new_dict

    def create_passes(self, passes: List[dict]):
        batch_url = "".join([self.api_url, BATCH_ENDPOINT])
        batch_result = self.batch_create_passes(passes, batch_url)
        batch_created_passes, batch_failed_passes = self.parse_batch_results(passes, batch_result)
        return batch_created_passes, batch_failed_passes

    @retry(delay=3, tries=3)
    def batch_create_passes(self, passes: List[dict], batch_url: str):
        batch_request_data = []
        for yp_pass in passes:
            pass_dict = self._get_post_dict_for_create_pass(yp_pass)
            batch_request_data.append(pass_dict)
        batch_request = requests.post(batch_url, headers=self.request_header, json=batch_request_data)
        return batch_request.json()

    @staticmethod
    def _get_post_dict_for_create_pass(pass_data: dict):
        batch_create_dict = {"method": "POST",
                             "data": pass_data}
        return batch_create_dict

    def parse_batch_results(self, passes: List[dict], batch_result: List[dict]):
        """
        check for created and not created passes return them in two lists
        """
        created = []
        failed = []
        for i, result in enumerate(batch_result):
            if result["status"]["code"] == 201 or result["status"]["code"] == 200:
                created.append(self.flatten_result(result["data"]))
            else:
                logging.warning(
                    f"Pass operation failed on : {passes[i]}\n because : {result['status']}"
                    f"Check if Pass Id is valid and exists, and all necessary data is being sent")
                failed.append(self.flatten_result(passes[i]))
        return created, failed

    def flatten_result(self, json_dict: dict, delim: str = "_"):
        flattened_dict = {}
        for main_key in json_dict.keys():
            if isinstance(json_dict[main_key], dict):
                get = self.flatten_result(json_dict[main_key], delim)
                for sub_key in get.keys():
                    new_key = "".join([main_key, delim, sub_key])
                    flattened_dict[new_key] = get[sub_key]
            else:
                flattened_dict[main_key] = json_dict[main_key]

        return flattened_dict

    def delete_passes(self, passes):
        batch_url = "".join([self.api_url, BATCH_ENDPOINT])
        pass_ids = self.get_pass_ids(passes)
        batch_result = self.batch_delete_passes(pass_ids, batch_url)
        batch_created_passes, batch_failed_passes = self.parse_batch_results(passes, batch_result)
        return batch_created_passes, batch_failed_passes

    @retry(delay=3, tries=3)
    def batch_delete_passes(self, passes: List[dict], batch_url: str):
        batch_request_data = []
        for yp_pass in passes:
            pass_dict = self._get_post_dict_for_delete_pass(yp_pass)
            batch_request_data.append(pass_dict)
        batch_request = requests.post(batch_url, headers=self.request_header, json=batch_request_data)
        return batch_request.json()

    @staticmethod
    def _get_post_dict_for_delete_pass(id: str):
        batch_delete_dict = {"method": "DELETE",
                             "id": id}
        return batch_delete_dict

    @staticmethod
    def get_pass_ids(passes: List[dict]):
        return [yp_pass["id"] for yp_pass in passes]

    def update_passes(self, passes: List[dict]):
        batch_url = "".join([self.api_url, BATCH_ENDPOINT])
        batch_result = self.batch_update_passes(passes, batch_url)
        updated_passes, batch_failed_passes = self.parse_batch_results(passes, batch_result)
        return updated_passes, batch_failed_passes

    @retry(delay=3, tries=3)
    def batch_update_passes(self, pass_data: List[dict], batch_url: str):
        batch_request_data = []
        for yp_pass in pass_data:
            pass_dict = self._get_post_dict_for_update_pass(yp_pass)
            batch_request_data.append(pass_dict)
        batch_request = requests.post(batch_url, headers=self.request_header, json=batch_request_data)
        return batch_request.json()

    @staticmethod
    def _get_post_dict_for_update_pass(pass_data: dict):
        update_data = {key: pass_data[key] for key in UPDATE_KEYS if key in pass_data}
        batch_update_dict = {"method": "PUT",
                             "id": pass_data["id"],
                             "data": update_data}
        return batch_update_dict
