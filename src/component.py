import csv
import logging

from yourpass import YourPassClient, LoginError
from keboola.component.base import ComponentBase, UserException
from keboola.component.dao import TableDefinition
from typing import Callable, Generator, List

# configuration variables
KEY_USERNAME = "username"
KEY_PASSWORD = "#password"
KEY_CLIENT_ID = "client_id"
KEY_CLIENT_SECRET = "#client_secret"
KEY_ENDPOINT = "endpoint"
KEY_ACTION = "action"
KEY_SANDBOX = "sandbox"

BATCH_LIMIT = 100

REQUIRED_PARAMETERS = [KEY_USERNAME, KEY_PASSWORD, KEY_CLIENT_ID, KEY_ENDPOINT, KEY_ACTION]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):
    def __init__(self):
        super().__init__(required_parameters=REQUIRED_PARAMETERS,
                         required_image_parameters=REQUIRED_IMAGE_PARS)

    def run(self):
        params = self.configuration.parameters

        username = params.get(KEY_USERNAME)
        password = params.get(KEY_PASSWORD)
        client_id = params.get(KEY_CLIENT_ID)
        client_secret = params.get(KEY_CLIENT_SECRET, "")
        endpoint = params.get(KEY_ENDPOINT)
        action = params.get(KEY_ACTION)
        sandbox = params.get(KEY_SANDBOX)

        input_table = self.get_input_table()
        input_file_reader = self.get_input_file_reader(input_table)

        client = self.get_yourpass_client(client_id, client_secret, username, password, sandbox)

        endpoint_action = self.get_endpoint_action(client, endpoint, action)

        successful_results = []
        unsuccessful_results = []
        for chunk in self.get_chunks(input_file_reader, BATCH_LIMIT):
            chunk = [YourPassClient.unflatten_dynamic_data(yp_pass) for yp_pass in chunk]
            chunk_successful, chunk_unsuccessful = endpoint_action(chunk)
            successful_results.extend(chunk_successful)
            unsuccessful_results.extend(chunk_unsuccessful)

        if successful_results:
            successful_columns = list(successful_results[0].keys())
            table_name = "".join(["successful_", endpoint, "_", action, ".csv"])
            successful_table = self.create_out_table_definition(table_name, columns=successful_columns)
            self.write_results(successful_table.full_path, successful_results, successful_columns)
            self.write_tabledef_manifest(successful_table)

        if unsuccessful_results:
            unsuccessful_columns = list(unsuccessful_results[0].keys())
            table_name = "".join(["unsuccessful_", endpoint, "_", action, ".csv"])
            unsuccessful_table = self.create_out_table_definition(table_name, columns=unsuccessful_columns)
            self.write_results(unsuccessful_table.full_path, unsuccessful_results, unsuccessful_columns)
            self.write_tabledef_manifest(unsuccessful_table)

    @staticmethod
    def get_yourpass_client(client_id: str, client_secret: str, username: str, password: str,
                            sandbox: bool) -> YourPassClient:
        try:
            yourpass_client = YourPassClient(sandbox)
            yourpass_client.authenticate_client(client_id, client_secret, username, password)
            return yourpass_client
        except LoginError as login_error:
            raise UserException(login_error) from login_error

    def get_input_table(self) -> TableDefinition:
        input_tables = self.get_input_tables_definitions()
        if len(input_tables) == 0:
            raise UserException("No input table added. Please add an input table")
        elif len(input_tables) > 1:
            raise UserException("Too many input tables added. Please add only one input table")
        return input_tables[0]

    @staticmethod
    def get_endpoint_action(client: YourPassClient, endpoint: str, action: str) -> Callable:
        if endpoint == "Pass":
            if action == "Create":
                logging.info("Creating passes")
                return client.create_passes
            if action == "Update":
                logging.info("Updating passes")
                return client.update_passes
            if action == "Delete":
                logging.info("Deleting passes")
                return client.delete_passes

    @staticmethod
    def get_input_file_reader(input_table: TableDefinition) -> Generator:
        # Returns a generator of the input file, yields rows of file
        with open(input_table.full_path, mode='r') as in_file:
            reader = csv.DictReader(in_file)
            for input_row in reader:
                yield input_row

    @staticmethod
    def get_chunks(generator: Generator, chunk_size: int) -> Generator:
        # Returns a chunking generator of the input file, yields chunks of specified size
        # Chunks are lists of rows
        chunk = []
        for item in generator:
            if len(chunk) >= chunk_size:
                yield chunk
                chunk = [item]
            else:
                chunk.append(item)
        if chunk:
            yield chunk

    @staticmethod
    def write_results(table_path: str, passes: List, columns: List):
        with open(table_path, mode='wt', encoding='utf-8', newline='') as out_file:
            writer = csv.DictWriter(out_file, columns)
            for yp_pass in passes:
                writer.writerow(yp_pass)


if __name__ == "__main__":
    try:
        comp = Component()
        comp.run()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
